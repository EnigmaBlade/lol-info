package net.enigmablade.lolinfo;

import java.util.*;

/**
 * Champion information.
 * @author Enigma
 */
public class Champion
{
	private String name, id, key, title, blurb;
	private String[] tags;
	private Map<String, Float> stats;
	
	public static final String RATING_ATTACK = "attack", RATING_DEFENSE = "defense", RATING_MAGIC = "magic", RATING_DIFFICULTY = "difficulty";
	private Map<String, Integer> ratings;
	
	private ImageData image;
	
	protected Champion(String name, String id, String key, String title, String blurb, String[] tags, Map<String, Float> stats, Map<String, Integer> ratings, ImageData image)
	{
		this.name = name;
		this.id = id;
		this.key = key;
		this.title = title;
		this.blurb = blurb;
		this.tags = tags;
		this.stats = stats;
		this.ratings = ratings;
		this.image = image;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getID()
	{
		return id;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getBlurb()
	{
		return blurb;
	}
	
	public String[] getTags()
	{
		return tags;
	}
	
	public int getRating(String rating)
	{
		return ratings.get(rating);
	}
	
	public ImageData getImage()
	{
		return image;
	}
	
	public Map<String, Float> getStats()
	{
		return stats;
	}
}
