package net.enigmablade.lolinfo;

import java.io.*;
import java.net.*;
import java.util.*;
import net.enigmablade.jsonic.*;

/**
 * Handles the retrieval and storage of item information.
 * @author Enigma
 */
public class ItemInfo extends HashMap<String, Item>
{
	private String version;
	
	private ItemInfo(String version)
	{
		this.version = version;
	}
	
	//Retrieval methods
	
	public static ItemInfo getItems(Realm realm)
	{
		if(realm == null)
			throw new IllegalArgumentException("The realm cannot be null");
		
		System.out.println("Retrieving item data");
		ItemInfo itemData = null;
		HttpURLConnection connection = null;
		InputStream in = null;
		try
		{
			//Connect to server via HTTP
			String url = realm.getCDN()+"/"+realm.getVersion()+"/data/"+realm.getLanguage()+"/item.json";
			//System.out.println("\tConnecting: "+url);
			URL realmURL = new URL(url);
			connection = (HttpURLConnection)realmURL.openConnection();
			connection.connect();
			
			//Check HTTP response
			int response = connection.getResponseCode();
			//System.out.println("\tResponse: "+response);
			if(response == 200)
			{
				//System.out.println("\tConnected!");
				
				//Read realm info
				//System.out.println("\tReading item data\n");
				in = connection.getInputStream();
				Scanner scanner = new Scanner(in);
				scanner.useDelimiter("\\A");
				String json = scanner.next();
				scanner.close();
				
				JsonObject rootNode = JsonParser.parseObject(json, false);
				
				String type = rootNode.getString("type");
				//System.out.println("\tType: "+type);
				//Verify we're working with item data
				if("item".equals(type))
				{
					String version = rootNode.getString("version");
					//System.out.println("\tVersion: "+version);
					
					itemData = new ItemInfo(version);
					JsonObject dataNode = rootNode.getObject("data");
					for(String itemId : dataNode.keySet())
					{
						//System.out.println("\n\tParsing: "+itemId);
						try
						{
							JsonObject itemNode = dataNode.getObject(itemId);
							Item item = parseItem(itemNode, itemId);
							if(item != null)
								itemData.put(item.getID(), item);
						}
						catch(Exception e)
						{
							//System.out.println("Error: Failed to parse item information");
							e.printStackTrace();
						}
					}
				}
				else
				{
					//System.out.println("\tError: File type mismatch");
				}
			}
		}
		catch(JsonException e)
		{
			System.err.println("Error: Failed to parse item data");
			e.printStackTrace();
		}
		catch(MalformedURLException e)
		{
			System.err.println("Error: Failed to retrieve item data");
			e.printStackTrace();
		}
		catch(IOException e)
		{
			System.err.println("Error: Failed to retrieve item data");
			e.printStackTrace();
		}
		finally
		{
			if(in != null)
				try
				{
					in.close();
				}
				catch(IOException e){}
			if(connection != null)
				connection.disconnect();
		}
		
		System.out.println("Complete\n");
		return itemData;
	}
	
	private static Item parseItem(JsonObject itemNode, String itemId) throws JsonException
	{
		Item item = new Item(itemId, itemNode.getString("name"));
		
		return item;
	}
	
	//Accessor methods
	
	public String getVersion()
	{
		return version;
	}
}
