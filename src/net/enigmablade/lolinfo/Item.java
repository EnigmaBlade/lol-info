package net.enigmablade.lolinfo;

public class Item
{
	private String id;
	private String name;
	
	public Item(String id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public String getID()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
}
