package net.enigmablade.lolinfo;

import java.io.*;
import java.net.*;
import net.enigmablade.jsonic.*;

/**
 * Realm information and versions.
 * @author Enigma
 */
public class Realm
{
	private String language;
	private String cdn;
	private String version, ddVersion, lgVersion, cssVersion;
	private String itemVersion, runeVersion, masteryVersion, summonerVersion, championVersion, profileIconVersion, languageVersion;
	
	public static final String REALM_NA = "na", REALM_EUW = "euw", REALM_EUNE = "eune", REALM_BR = "br", REALM_KR = "kr", REALM_TR = "tr";
	
	private Realm(String language, String cdn, 
			String version, String ddVersion, String lgVersion, String cssVersion, 
			String itemVersion, String runeVersion, String masteryVersion, String summonerVersion, String championVersion, String profileIconVersion, String languageVersion)
	{
		this.version = version;
		this.ddVersion = ddVersion;
		this.lgVersion = lgVersion;
		this.cssVersion = cssVersion;
		this.itemVersion = itemVersion;
		this.runeVersion = runeVersion;
		this.masteryVersion = masteryVersion;
		this.summonerVersion = summonerVersion;
		this.championVersion = championVersion;
		this.profileIconVersion = profileIconVersion;
		this.languageVersion = languageVersion;
		this.language = language;
		this.cdn = cdn;
	}
	
	//Retrieval methods
	
	/**
	 * Retrieve the realm information and versions given the realm code.
	 * @param realmCode The realm code: na, euw, eune, br, kr, etc.
	 * @return The realm information and versions
	 */
	public static Realm getRealm(String realmCode)
	{
		System.out.println("Retrieving realm data");
		Realm realm = null;
		HttpURLConnection connection = null;
		InputStream in = null;
		try
		{
			//Connect to server via HTTP
			String url = "http://ddragon.leagueoflegends.com/realms/"+realmCode.toLowerCase()+".json";
			//System.out.println("\tConnecting: "+url);
			URL realmURL = new URL(url);
			connection = (HttpURLConnection)realmURL.openConnection();
			connection.connect();
			
			//Check HTTP response
			int response = connection.getResponseCode();
			System.out.println("\tResponse: "+response);
			if(response == 200)
			{
				//System.out.println("\tConnected!");
				
				//Read realm info
				//System.out.println("\tReading realm data\n");
				in = connection.getInputStream();
				JsonObject root = JsonParser.parseObject(in);
				in.close();
				
				JsonObject versionMap = root.getObject("n");
				String itemVersion = versionMap.getString("item");
				//System.out.println("\tItem version: "+itemVersion);
				String runeVersion = versionMap.getString("rune");
				//System.out.println("\tRune version: "+runeVersion);
				String masteryVersion = versionMap.getString("mastery");
				//System.out.println("\tMastery version: "+masteryVersion);
				String summonerVersion = versionMap.getString("summoner");
				//System.out.println("\tSummoner version: "+summonerVersion);
				String championVersion = versionMap.getString("champion");
				//System.out.println("\tChampion version: "+championVersion);
				String profileIconVersion = versionMap.getString("profileicon");
				//System.out.println("\tProfile icon version: "+profileIconVersion);
				String languageVersion = versionMap.getString("language");
				//System.out.println("\tLanguage version: "+languageVersion);
				
				String version = root.getString("v");
				//System.out.println("\tVersion: "+version);
				String language = root.getString("l");
				//System.out.println("\tLanguage: "+language);
				String cdn = root.getString("cdn").replace("\\/", "/");
				//System.out.println("\tCDN: "+cdn);
				String ddVersion = root.getString("dd");
				//System.out.println("\tDD Version: "+ddVersion);
				String lgVersion = root.getString("lg");
				//System.out.println("\tLG Version: "+lgVersion);
				String cssVersion = root.getString("css");
				//System.out.println("\tCSS Version: "+cssVersion);
				
				realm = new Realm(language, cdn, version, ddVersion, lgVersion, cssVersion, itemVersion, runeVersion, masteryVersion, summonerVersion, championVersion, profileIconVersion, languageVersion);
			}
		}
		catch(JsonException e)
		{
			e.printStackTrace();
		}
		catch(MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(in != null)
				try
			{
					in.close();
			}
			catch(IOException e){}
			if(connection != null)
				connection.disconnect();
		}
		
		System.out.println("Complete\n");
		return realm;
	}
	
	//Accessor methods
	
	public String getLanguage()
	{
		return language;
	}
	
	public String getCDN()
	{
		return cdn;
	}
	
	public String getVersion()
	{
		return version;
	}
	
	public String getDDVersion()
	{
		return ddVersion;
	}
	
	public String getLGVersion()
	{
		return lgVersion;
	}
	
	public String getCSSVersion()
	{
		return cssVersion;
	}
	
	public String getItemVersion()
	{
		return itemVersion;
	}
	
	public String getRuneVersion()
	{
		return runeVersion;
	}
	
	public String getMasteryVersion()
	{
		return masteryVersion;
	}
	
	public String getSummonerVersion()
	{
		return summonerVersion;
	}
	
	public String getChampionVersion()
	{
		return championVersion;
	}
	
	public String getProfileIconVersion()
	{
		return profileIconVersion;
	}
	
	public String getLanguageVersion()
	{
		return languageVersion;
	}
}
