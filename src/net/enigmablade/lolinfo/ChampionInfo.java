package net.enigmablade.lolinfo;

import java.io.*;
import java.net.*;
import java.util.*;
import net.enigmablade.jsonic.*;

/**
 * Handles the retrieval and storage of Champion information.
 * @author Enigma
 */
public class ChampionInfo extends HashMap<String, Champion>
{
	private String version;
	
	private ChampionInfo(String version)
	{
		this.version = version;
	}
	
	//Retrieval methods
	
	/**
	 * Download and parse Champion data from the Riot CDN with the given realm (region).
	 * @param realm The realm (region) from which to retrieve Champion data
	 * @return The downloaded and parsed Champion data
	 */
	public static ChampionInfo getChampions(Realm realm)
	{
		if(realm == null)
			throw new IllegalArgumentException("The realm cannot be null");
		
		System.out.println("Retrieving champion data");
		ChampionInfo championData = null;
		HttpURLConnection connection = null;
		InputStream in = null;
		try
		{
			//Connect to server via HTTP
			String url = realm.getCDN()+"/"+realm.getVersion()+"/data/"+realm.getLanguage()+"/champion.json";
			//System.out.println("\tConnecting: "+url);
			URL realmURL = new URL(url);
			connection = (HttpURLConnection)realmURL.openConnection();
			connection.connect();
			
			//Check HTTP response
			int response = connection.getResponseCode();
			//System.out.println("\tResponse: "+response);
			if(response == 200)
			{
				//System.out.println("\tConnected!");
				
				//Read realm info
				//System.out.println("\tReading champion data\n");
				in = connection.getInputStream();
				Scanner scanner = new Scanner(in);
				scanner.useDelimiter("\\A");
				String json = scanner.next();
				scanner.close();
				
				JsonObject rootNode = JsonParser.parseObject(json);
				
				String type = rootNode.getString("type");
				//System.out.println("\tType: "+type);
				//Verify we're working with champion data
				if("champion".equals(type))
				{
					//String format = (String)root.get("format"); //I would assume this has to do with the specific layout of the JSON
					String version = rootNode.getString("version");
					//System.out.println("\tVersion: "+version);
					
					championData = new ChampionInfo(version);
					JsonObject dataNode = rootNode.getObject("data");
					for(String dataKey : dataNode.keySet())
					{
						//System.out.println("\n\tParsing: "+dataKey);
						try
						{
							JsonObject championNode = dataNode.getObject(dataKey);
							Champion champion = parseChampion(championNode, dataKey);
							if(champion != null)
							{
								System.out.println("Adding champion: "+champion.getKey());
								championData.put(champion.getKey(), champion);
							}
						}
						catch(Exception e)
						{
							System.out.println("Error: Failed to parse Champion information");
							e.printStackTrace();
						}
					}
				}
				else
				{
					System.out.println("\tError: File type mismatch");
				}
			}
		}
		catch(JsonException e)
		{
			System.err.println("Error: Failed to parse champion data");
			e.printStackTrace();
		}
		catch(MalformedURLException e)
		{
			System.err.println("Error: Failed to retrieve champion data");
			e.printStackTrace();
		}
		catch(IOException e)
		{
			System.err.println("Error: Failed to retrieve champion data");
			e.printStackTrace();
		}
		finally
		{
			if(in != null)
				try
				{
						in.close();
				}
				catch(IOException e){}
			if(connection != null)
				connection.disconnect();
		}
		
		System.out.println("Complete\n");
		return championData;
	}
	
	/**
	 * Parse the Champion data in the JSON object/node into a Champion object.
	 * @param championNode JSON object/node
	 * @param nodeKey The key of the object
	 * @return The parsed Champion data
	 */
	private static Champion parseChampion(JsonObject championNode, String nodeKey) throws JsonException
	{
		Champion champion = null;
		
		//String cVersion = (String)champion.get("version"); //Don't know how this could be used, maybe check against the file version?
		String cName = championNode.getString("name");	//Ex: Dr. Mundo
		//System.out.println("\tName: \""+cName+"\"");
		String cID = championNode.getString("id");		//Ex: DrMundo, should be same as dataKey
		//System.out.println("\tID: \""+cID+"\"");
		String cKey = championNode.getString("key");	//Ex: 36
		//System.out.println("\tKey: \""+cKey+"\"");
		
		//Verify the keys match and continue
		if(nodeKey.equals(cID))
		{
			String cTitle = championNode.getString("title");
			//System.out.println("\tTitle: \""+cTitle+"\"");
			String cBlurb = championNode.getString("blurb");
			//System.out.println("\tBlurb: \""+cBlurb+"\"");
			
			//Info/ratings
			JsonObject infoNode = championNode.getObject("info");
			Map<String, Integer> ratings = new HashMap<String, Integer>();
			//System.out.println("\tRatings:");
			for(Object ratingO : infoNode.keySet())
			{
				String ratingKey = (String)ratingO;
				int rating = (int)(long)infoNode.get(ratingKey);
				//System.out.println("\t\t"+ratingKey+": "+rating);
				ratings.put(ratingKey, rating);
			}
			
			//Image
			JsonObject imageNode = championNode.getObject("image");
			//System.out.println("\tImage:");
			String imgFull = imageNode.getString("full");
			//System.out.println("\t\tFull: "+imgFull);
			String imgSprite = imageNode.getString("sprite");
			//System.out.println("\t\tSprite: "+imgSprite);
			String imgGroup = imageNode.getString("group");
			//System.out.println("\t\tGroup: "+imgGroup);
			int imgX = imageNode.getInt("x");
			//System.out.println("\t\tX: "+imgX);
			int imgY = imageNode.getInt("y");
			//System.out.println("\t\tY: "+imgY);
			int imgW = imageNode.getInt("w");
			//System.out.println("\t\tW: "+imgW);
			int imgH = imageNode.getInt("h");
			//System.out.println("\t\tH: "+imgH);
			ImageData image = new ImageData(imgFull, imgSprite, imgGroup, imgX, imgY, imgW, imgH);
			
			//Tags
			JsonArray tagsArray = championNode.getArray("tags");
			List<String> newTags = new ArrayList<String>(tagsArray.size());
			//Clean-up tags: filter out empty and "null", trim and capitalize
			for(int n = 0; n < tagsArray.size(); n++)
			{
				String tag = tagsArray.getString(n).trim();
				if(tag.length() > 0 && !tag.equals("null"))
					newTags.add(tag.substring(0, 1).toUpperCase()+(tag.length() > 1 ? tag.substring(1) : ""));
			}
			String[] tags = newTags.toArray(new String[newTags.size()]);
			//System.out.println("\tTags: "+Arrays.toString(tags));
			
			//Stats
			JsonObject statsNode = championNode.getObject("stats");
			Map<String, Float> stats = new HashMap<String, Float>();
			//System.out.println("\tStats:");
			for(Object statO : statsNode.keySet())
			{
				String statKey = (String)statO;
				float stat = (float)(double)statsNode.get(statKey);
				//System.out.println("\t\t"+statKey+": "+stat);
				stats.put(statKey, stat);
			}
			
			champion = new Champion(cName, cID, cKey, cTitle, cBlurb, tags, stats, ratings, image);
		}
		else
		{
			System.out.println("\tError: Key mismatch");
		}
		
		return champion;
	}
	
	//Accessor methods
	
	public String getVersion()
	{
		return version;
	}
}
