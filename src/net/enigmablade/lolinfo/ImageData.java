package net.enigmablade.lolinfo;

/**
 * Image information for Champions, items, masteries, and runes.
 * @author Enigma
 */
public class ImageData
{
	private String image, sprite, group;
	private int x, y, w, h;
	
	protected ImageData(String image, String sprite, String group, int x, int y, int w, int h)
	{
		this.image = image;
		this.sprite = sprite;
		this.group = group;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public String getImage()
	{
		return image;
	}
	
	public String getSprite()
	{
		return sprite;
	}
	
	public String getGroup()
	{
		return group;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public int getWidth()
	{
		return w;
	}
	
	public int getHeight()
	{
		return h;
	}
}
