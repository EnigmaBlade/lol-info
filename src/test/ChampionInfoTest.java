package test;

import org.junit.*;
import static org.junit.Assert.*;
import net.enigmablade.lolinfo.*;

public class ChampionInfoTest
{
	//Will need to be updated as Champions are released
	private static final String[] CHAMPIONS = {
		"Aatrox", "Ahri", "Akali", "Alistar", "Amumu", "Anivia", "Annie", "Ashe", "Blitzcrank", "Brand", "Caitlyn",
		"Cassiopeia", "Chogath", "Corki", "Darius", "Diana", "DrMundo", "Draven", "Elise", "Evelynn", "Ezreal",
		"FiddleSticks", "Fiora", "Fizz", "Galio", "Gangplank", "Garen", "Gragas", "Graves", "Hecarim",
		"Heimerdinger", "Irelia", "Janna", "JarvanIV", "Jax", "Jayce", "Jinx", "Karma", "Karthus", "Kassadin",
		"Katarina", "Kayle", "Kennen", "Khazix", "KogMaw", "Leblanc", "LeeSin", "Leona", "Lissandra", "Lucian", "Lulu", "Lux",
		"Malphite", "Malzahar", "Maokai", "MasterYi", "MissFortune", "MonkeyKing", "Mordekaiser", "Morgana",
		"Nami", "Nasus", "Nautilus", "Nidalee", "Nocturne", "Nunu", "Olaf", "Orianna", "Pantheon", "Poppy", "Quinn",
		"Rammus", "Renekton", "Rengar", "Riven", "Rumble", "Ryze", "Sejuani", "Shaco", "Shen", "Shyvana",
		"Singed", "Sion", "Sivir", "Skarner", "Sona", "Soraka", "Swain", "Syndra", "Talon", "Taric", "Teemo", "Thresh",
		"Tristana", "Trundle", "Tryndamere", "TwistedFate", "Twitch", "Udyr", "Urgot", "Varus", "Vayne",
		"Veigar", "Vi", "Viktor", "Vladimir", "Volibear", "Warwick", "Xerath", "XinZhao", "Yorick", "Zac", "Zed",
		"Ziggs", "Zilean", "Zyra"
	};
	
	private Realm realm;
	
	@Before
	public void setUp() throws Exception
	{
		realm = Realm.getRealm(Realm.REALM_NA);
	}
	
	@Test
	public void testGetChampions()
	{
		ChampionInfo info = ChampionInfo.getChampions(realm);
		
		//Make sure it loaded all the Champions
		assertEquals(CHAMPIONS.length, info.size());
		for(String championKey : CHAMPIONS)
		{
			assertTrue(info.containsKey(championKey));
			assertNotNull(info.get(championKey));
		}
		
		//Check certain values on all Champions
		for(String championKey : CHAMPIONS)
		{
			assertEquals(20, info.get(championKey).getStats().size());
		}
		
		//Check a few select Champions since keeping tests updated with balance changes would be a pain
		Champion ahri = info.get("Ahri");
		assertEquals("Ahri", ahri.getName());
		assertEquals("Ahri", ahri.getID());
		assertEquals("103", ahri.getKey());
		assertEquals("the Nine-Tailed Fox", ahri.getTitle());
		//Ratings
		assertEquals(3, ahri.getRating(Champion.RATING_ATTACK));
		assertEquals(4, ahri.getRating(Champion.RATING_DEFENSE));
		assertEquals(8, ahri.getRating(Champion.RATING_MAGIC));
		assertEquals(8, ahri.getRating(Champion.RATING_DIFFICULTY));
		//Image
		assertNotNull(ahri.getImage());
		assertEquals("Ahri.png", ahri.getImage().getImage());
		assertEquals("champion0.png", ahri.getImage().getSprite());
		assertEquals("champion", ahri.getImage().getGroup());
		assertEquals(48, ahri.getImage().getX());
		assertEquals(0, ahri.getImage().getY());
		assertEquals(48, ahri.getImage().getWidth());
		assertEquals(48, ahri.getImage().getHeight());
		//Tags
		assertArrayEquals(new String[]{"Mage"}, ahri.getTags());
		//Select stats
		assertEquals(550.0f, ahri.getStats().get("attackrange"), 1);
		assertEquals(50.0f, ahri.getStats().get("attackdamage"), 0.1);
		assertEquals(3.0f, ahri.getStats().get("attackdamageperlevel"), 0.1);
		assertEquals(330.0f, ahri.getStats().get("movespeed"), 1);
		assertEquals(30.0f, ahri.getStats().get("spellblock"), 0.1);
		assertEquals(0.0f, ahri.getStats().get("spellblockperlevel"), 0.1);
		
		Champion rammus = info.get("Rammus");
		assertEquals("Rammus", rammus.getName());
		assertEquals("Rammus", rammus.getID());
		assertEquals("33", rammus.getKey());
		assertEquals("the Armordillo", rammus.getTitle());
		//Ratings
		assertEquals(4, rammus.getRating(Champion.RATING_ATTACK));
		assertEquals(10, rammus.getRating(Champion.RATING_DEFENSE));
		assertEquals(5, rammus.getRating(Champion.RATING_MAGIC));
		assertEquals(5, rammus.getRating(Champion.RATING_DIFFICULTY));
		//Image
		assertNotNull(rammus.getImage());
		assertEquals("Rammus.png", rammus.getImage().getImage());
		assertEquals("champion2.png", rammus.getImage().getSprite());
		assertEquals("champion", rammus.getImage().getGroup());
		assertEquals(0, rammus.getImage().getX());
		assertEquals(48, rammus.getImage().getY());
		assertEquals(48, rammus.getImage().getWidth());
		assertEquals(48, rammus.getImage().getHeight());
		//Tags
		assertArrayEquals(new String[]{"Tank"}, rammus.getTags());
		//Select stats
		assertEquals(125.0f, rammus.getStats().get("attackrange"), 1);
		assertEquals(50.0f, rammus.getStats().get("attackdamage"), 0.1);
		assertEquals(3.5f, rammus.getStats().get("attackdamageperlevel"), 0.1);
		assertEquals(335.0f, rammus.getStats().get("movespeed"), 1);
		assertEquals(30.0f, rammus.getStats().get("spellblock"), 0.1);
		assertEquals(1.25f, rammus.getStats().get("spellblockperlevel"), 0.01);
	}
	
	/*@Test
	public void testGetVersion()
	{
		ChampionInfo info = ChampionInfo.getChampions(realm);
		assertEquals("3.14.41", info.getVersion());
	}*/
}
