package test;

import org.junit.*;
import static org.junit.Assert.*;
import net.enigmablade.lolinfo.*;

/**
 * Tests for Realm.
 * @author Enigma
 */
public class RealmTest
{
	/**
	 * Note: Test results may fail over time due to changing version values.
	 */
	@Test
	public void testGetRealm()
	{
		//Null case: Invalid realm
		Realm realmNull = Realm.getRealm("completely_invalid_realm");
		assertNull(realmNull);
		
		//Basic case: NA realm, lower-case
		Realm realmNA = Realm.getRealm(Realm.REALM_NA);
		assertNotNull(realmNA);
		assertEquals("en_US", realmNA.getLanguage());
		assertEquals("http://ddragon.leagueoflegends.com/cdn", realmNA.getCDN());
		
		assertEquals("3.14.41", realmNA.getVersion());
		assertEquals("3.14.41", realmNA.getDDVersion());
		assertEquals("0.152.55", realmNA.getLGVersion());
		assertEquals("0.152.55", realmNA.getCSSVersion());
		
		assertEquals("3.14.41", realmNA.getItemVersion());
		assertEquals("3.14.41",  realmNA.getRuneVersion());
		assertEquals("3.14.41", realmNA.getMasteryVersion());
		assertEquals("3.14.41", realmNA.getSummonerVersion());
		assertEquals("3.14.41", realmNA.getChampionVersion());
		assertEquals("3.14.41", realmNA.getProfileIconVersion());
		assertEquals("3.14.41", realmNA.getLanguageVersion());
		
		//Basic case: NA realm, upper-case
		Realm realmNA2 = Realm.getRealm("NA");
		assertNotNull(realmNA2);
		assertEquals("en_US", realmNA2.getLanguage());
		assertEquals("3.14.41", realmNA2.getVersion());
		
		//Basic case: NA realm, mixed-case
		Realm realmNA3 = Realm.getRealm("Na");
		assertNotNull(realmNA3);
		assertEquals("en_US", realmNA3.getLanguage());
		assertEquals("3.14.41", realmNA3.getVersion());
		
		//Basic case: EUW realm
		Realm realmEUW = Realm.getRealm(Realm.REALM_EUW);
		assertNotNull(realmEUW);
		assertEquals("en_US", realmEUW.getLanguage());
		assertEquals("http://ddragon.leagueoflegends.com/cdn", realmEUW.getCDN());
		
		assertEquals("3.14.41", realmEUW.getVersion());
		assertEquals("3.14.41", realmEUW.getDDVersion());
		assertEquals("0.152.55", realmEUW.getLGVersion());
		assertEquals("0.152.55", realmEUW.getCSSVersion());
		
		assertEquals("3.14.41", realmEUW.getItemVersion());
		assertEquals("3.14.41",  realmEUW.getRuneVersion());
		assertEquals("3.14.41", realmEUW.getMasteryVersion());
		assertEquals("3.14.41", realmEUW.getSummonerVersion());
		assertEquals("3.14.41", realmEUW.getChampionVersion());
		assertEquals("3.14.41", realmEUW.getProfileIconVersion());
		assertEquals("3.14.41", realmEUW.getLanguageVersion());
		
		//Case: EUNE realm
		Realm realmEUNE = Realm.getRealm(Realm.REALM_EUNE);
		assertNotNull(realmEUNE);
		assertEquals("en_US", realmEUNE.getLanguage());
		assertEquals("http://ddragon.leagueoflegends.com/cdn", realmEUNE.getCDN());
		
		assertEquals("3.14.41", realmEUNE.getVersion());
		assertEquals("3.14.41", realmEUNE.getDDVersion());
		assertEquals("0.152.55", realmEUNE.getLGVersion());
		assertEquals("0.152.55", realmEUNE.getCSSVersion());
		
		assertEquals("3.14.41", realmEUNE.getItemVersion());
		assertEquals("3.14.41",  realmEUNE.getRuneVersion());
		assertEquals("3.14.41", realmEUNE.getMasteryVersion());
		assertEquals("3.14.41", realmEUNE.getSummonerVersion());
		assertEquals("3.14.41", realmEUNE.getChampionVersion());
		assertEquals("3.14.41", realmEUNE.getProfileIconVersion());
		assertEquals("3.14.41", realmEUNE.getLanguageVersion());
		
		//Case: BR (Brazilian) realm
		Realm realmBR = Realm.getRealm(Realm.REALM_BR);
		assertNotNull(realmBR);
		assertEquals("pt_BR", realmBR.getLanguage());
		assertEquals("http://ddragon.leagueoflegends.com/cdn", realmBR.getCDN());
		
		assertEquals("3.14.41", realmBR.getVersion());
		assertEquals("3.14.41", realmBR.getDDVersion());
		assertEquals("0.152.55", realmBR.getLGVersion());
		assertEquals("0.152.55", realmBR.getCSSVersion());
		
		assertEquals("3.14.41", realmBR.getItemVersion());
		assertEquals("3.14.41",  realmBR.getRuneVersion());
		assertEquals("3.14.41", realmBR.getMasteryVersion());
		assertEquals("3.14.41", realmBR.getSummonerVersion());
		assertEquals("3.14.41", realmBR.getChampionVersion());
		assertEquals("3.14.41", realmBR.getProfileIconVersion());
		assertEquals("3.14.41", realmBR.getLanguageVersion());
		
		//Case: KR (Korean)
		Realm realmKR = Realm.getRealm(Realm.REALM_KR);
		assertNotNull(realmKR);
		assertEquals("ko_KR", realmKR.getLanguage());
		assertEquals("http://ddragon.leagueoflegends.com/cdn", realmKR.getCDN());
		
		assertEquals("3.14.41", realmKR.getVersion());
		assertEquals("3.14.41", realmKR.getDDVersion());
		assertEquals("0.152.55", realmKR.getLGVersion());
		assertEquals("0.152.55", realmKR.getCSSVersion());
		
		assertEquals("3.14.41", realmKR.getItemVersion());
		assertEquals("3.14.41",  realmKR.getRuneVersion());
		assertEquals("3.14.41", realmKR.getMasteryVersion());
		assertEquals("3.14.41", realmKR.getSummonerVersion());
		assertEquals("3.14.41", realmKR.getChampionVersion());
		assertEquals("3.14.41", realmKR.getProfileIconVersion());
		assertEquals("3.14.41", realmKR.getLanguageVersion());
		
		//Case: TR (Turkish) realm
		Realm realmTR = Realm.getRealm(Realm.REALM_TR);
		assertNotNull(realmTR);
		assertEquals("tr_TR", realmTR.getLanguage());
		assertEquals("http://ddragon.leagueoflegends.com/cdn", realmTR.getCDN());
		
		assertEquals("3.14.41", realmTR.getVersion());
		assertEquals("3.14.41", realmTR.getDDVersion());
		assertEquals("0.152.55", realmTR.getLGVersion());
		assertEquals("0.152.55", realmTR.getCSSVersion());
		
		assertEquals("3.14.41", realmTR.getItemVersion());
		assertEquals("3.14.41",  realmTR.getRuneVersion());
		assertEquals("3.14.41", realmTR.getMasteryVersion());
		assertEquals("3.14.41", realmTR.getSummonerVersion());
		assertEquals("3.14.41", realmTR.getChampionVersion());
		assertEquals("3.14.41", realmTR.getProfileIconVersion());
		assertEquals("3.14.41", realmTR.getLanguageVersion());
	}
}
